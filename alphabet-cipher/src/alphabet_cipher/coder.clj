(ns alphabet-cipher.coder
  (:use [clojure.string :only (join)]))

(defn- cipher-function [op chr key] 
    (let [a (int \a)
          c (- (int chr) a)
          k (- (int key) a)]
          (char (+ a (mod (op c k) 26)))))

(defn- cipher [[chr key]]
  (cipher-function + chr key))

(defn- uncipher [[chr key]]
  (cipher-function - chr key))

(defn- map-cipher [cipher-f keyword message]
  (let [n (count message)
        cyc (cycle keyword)
        keys (take n cyc)]
        (join "" (map cipher-f (map vector message keys)))))

(defn encode [keyword message]
  (map-cipher cipher keyword message))

(defn decode [keyword message]
  (map-cipher uncipher keyword message))

(defn- cipher-characters [cipher message]
  (map uncipher (map vector cipher message)))

(defn decipher [cipher message]
  (let [cc (cipher-characters cipher message)]
    (loop [n 1]
      (let [check (join "" (take n cc))]
        (cond (= cipher (encode check message)) check
              (> n (count check)) (throw (Exception. "Invalid cipher or message"))
              :else (recur (inc n)))))))

