(ns card-game-war.game)

(defn- index-of [elem coll]
  (first (keep-indexed #(if (= elem %2) %1) coll)))

(def player1 "Player 1")
(def player2 "Player 2")

;; feel free to use these cards or use your own data structure
(def suits [:spade :club :diamond :heart])
(def ranks [2 3 4 5 6 7 8 9 10 :jack :queen :king :ace])
(def cards
  (for [suit suits
        rank ranks]
    [suit rank]))

(defn- rank-strength [rank]
  (index-of rank ranks))

(defn- suit-strength [suit]
  (index-of suit suits))

(defn play-round [player1-card player2-card]
  (let [[s1 r1] player1-card
        [s2 r2] player2-card]
      (if (= r1 r2)
        (if (< (suit-strength s1) (suit-strength s2)) :player2, :player1)
        (if (< (rank-strength r1) (rank-strength r2)) :player2, :player1))))

(defn game-over [player]
  (println (str "Congrats, " player ". You win.")))

(defn log [card1 card2 cards1 cards2 res]
  (println "Player 1: " card1, "\tPlayer 2:", card2)
  (println "Player 1: " (count cards), " cards", "\tPlayer 2:", (count cards2), " cards")
  (print "Result\t=> ")
  (if (res == :player1) (println player1)
                        (println player2))
  (println "..................."))
(defn play-game [player1-cards player2-cards]
  (loop [p1 player1-cards
         p2 player2-cards]
    (cond (empty? p1) (game-over player2)
          (empty? p2) (game-over player1)
          :else
          (let [c1 (first p1) 
                c2 (first p2)
                r1 (rest p1)
                r2 (rest p2)
                res (play-round c1 c2)]
              (log c1 c2 p1 p2 res)
            (if (= :player1 res) (recur (concat r1 [c1 c2]) (rest p2))
                                 (recur (rest p1) (concat r2 [c2 c1])))))))

(defn game-loop []
  (let [[player1 player2] (partition 26 (shuffle cards))]
    (play-game player1 player2)))