(ns card-game-war.game-test
  (:require [clojure.test :refer :all]
            [card-game-war.game :refer :all]))


;; fill in  tests for your game
(deftest test-play-round
  (testing "the highest rank wins the cards in the round"
    (is (= :player1 (play-round [:spade 3] [:spade 2])))
    (is (= :player2 (play-round [:spade 2] [:spade 3]))))
  (testing "queens are higher rank than jacks"
    (is (= :player1 (play-round [:spade :queen] [:spade :jack])))
    (is (= :player2 (play-round [:spade :jack] [:spade :queen]))))
  (testing "kings are higher rank than queens"
    (is (= :player1 (play-round [:spade :king] [:spade :queen])))
    (is (= :player2 (play-round [:spade :queen] [:spade :king]))))
  (testing "aces are higher rank than kings"
    (is (= :player1 (play-round [:spade :ace] [:spade :king])))
    (is (= :player2 (play-round [:spade :king] [:spade :ace]))))
  (testing "if the ranks are equal, clubs beat spades"
    (is (= :player1 (play-round [:club 2] [:spade 2])))
    (is (= :player2 (play-round [:spade 2] [:club 2]))))
  (testing "if the ranks are equal, diamonds beat clubs"
    (is (= :player1 (play-round [:diamond 2] [:club 2])))
    (is (= :player2 (play-round [:club 2] [:diamond 2]))))
  (testing "if the ranks are equal, hearts beat diamonds"
    (is (= :player1 (play-round [:heart 2] [:diamond 2])))
    (is (= :player2 (play-round [:diamond 2] [:heart 2])))))

(deftest test-play-game
  (testing "the player loses when they run out of cards"))

